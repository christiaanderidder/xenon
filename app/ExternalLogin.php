<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\ExternalLogin;

class ExternalLogin extends Model {

	protected $table = 'external_logins';
	protected $fillable = ['driver', 'external_id'];
	protected $hidden = ['token'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
