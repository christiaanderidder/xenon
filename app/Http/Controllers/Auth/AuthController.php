<?php namespace App\Http\Controllers\Auth;

use App\User;
use App\ExternalLogin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Laravel\Socialite\Contracts\Factory as Socialite;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	private $auth;
	private $registrar;
	private $socialite;
	private $drivers = ['facebook', 'twitter', 'google', 'github'];

	public function __construct(Guard $auth, Registrar $registrar, Socialite $socialite)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->socialite = $socialite;
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Redirect to an external login partner and handle the callback.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getExternal($driver, Request $request)
	{
		if(!in_array($driver, $this->drivers))
		{
			// Invalid auth driver
			return redirect('/auth/login')->withErrors(['message' => 'Invalid external auth driver.']);
		}

		
		if(!$request->has('code') && !$request->has('oauth_token'))
		{
			// No code (oauth 2.0) or oauth_token (oauth 1.0), user needs to authenticate
			return $this->socialite->driver($driver)->redirect();
		}

		try
		{
			$externalUser = $this->socialite->driver($driver)->user();
		}
		catch(\Exception $e)
		{
			return redirect('/auth/login')->withErrors(['message' => 'External login failed.']);
		}

		if(!$externalUser)
		{
			// Invalid auth driver
			return redirect('/auth/login')->withErrors(['message' => 'External login failed.']);
		}

		$externalLogin = ExternalLogin::where('driver', $driver)->where('external_id', $externalUser->id)->first();
		if(!$externalLogin)
		{
			// External account has not been linked to any user
			return redirect('/auth/login')->withErrors(['message' => 'The '.$driver.' account \''.$externalUser->id.'\' has not been linked to any user.']);
		}

		$externalLogin->token = $externalUser->token;
		$externalLogin->save();

		$this->auth->loginUsingId($externalLogin->user_id);

		return redirect($this->redirectPath());	
	}

	/**
	 * Show the application registration form.
	 *
	 * @return Response
	 */
	public function getRegister()
	{
		return view('auth.register');
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  RegisterRequest  $request
	 * @return Response
	 */
	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$this->auth->login($this->registrar->create($request->all()));

		return redirect($this->redirectPath());
	}

	/**
	 * Show the application login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin()
	{
		return view('auth.login');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			return redirect($this->redirectPath());
		}

		return redirect('/auth/login')
					->withInput($request->only('email'))
					->withErrors([
						'email' => 'These credentials do not match our records.',
					]);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return Response
	 */
	public function getLogout()
	{
		$this->auth->logout();

		return redirect('/');
	}

	/**
	 * Get the post register / login redirect path.
	 *
	 * @return string
	 */
	public function redirectPath()
	{
		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
	}

}
