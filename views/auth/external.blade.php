<div class="btn-group btn-group-justified btn-group-lg">
	<a href="/auth/external/facebook" class="btn btn-default"><i class="fa fa-facebook"></i> Facebook</a>
	<a href="/auth/external/twitter" class="btn btn-default"><i class="fa fa-twitter"></i> Twitter</a>
	<a href="/auth/external/google" class="btn btn-default"><i class="fa fa-google"></i> Google</a>
	<a href="/auth/external/github" class="btn btn-default"><i class="fa fa-github"></i> Github</a>
</div>